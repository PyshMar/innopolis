"use scrict";

//Упражнение 1

let n = prompt('Введите число');
    
let intervalId = setInterval(function(){
if (Number.isNaN(n)) {
    clearInterval(intervalId);
    console.log("Ошибка, вы ввели не число");
} else {
    console.log('Осталось ' + n );
    n = n - 1;
}
if (n === 0) {
    clearInterval(intervalId)
    console.log('Время вышло!');
}
}, 1000);

//Упражнение 2
console.time('Time');
let promise = fetch("https://reqres.in/api/users");
promise
.then (function(response){
    return response.json();
})
.then(function(response){
    let users = response.data;
    console.log(`Получили пользователей: ${users.length}`);
    users.forEach(function(user){
        console.log(`- ${user.first_name} ${user.last_name} (${user.email})`);
    });
})
.catch(function(){
    console.log("Что-то пошло не так");
});
console.timeEnd('Time');
