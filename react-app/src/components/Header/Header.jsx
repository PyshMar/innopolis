import React from "react";
import "../../../src/components/Header/Header.css";
import icon from '../Header/icon.svg';
import heart from '../../icon/heart.svg';
import basket from "../../icon/basketGrey.svg";
function Header(){
    return(
        <>
            <div className="background-header">
                <div className="header-conteiner">
                    <div className="header">
                        <div className="header__name">
                            <img src={icon} id="start" alt="логотип"/>
                            <h1 className="title"><span className="text-logo">Мой</span>Маркет</h1>
                        </div>
                        <div className="header__elements">
                            <div className="header__elements_favorite">
                                <img src={heart} alt="Избранное"/>
                                <span className="header__elements_amount">12</span>
                            </div>
                            <div className="header__elements_basket">
                                <img src={basket} alt="Избранное"/>
                                <span className="basket__elements_amount" value= "1243" name="basket"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </> 
    );
}
export default Header;