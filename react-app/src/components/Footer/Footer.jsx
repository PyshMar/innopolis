import React from "react";
import "../../../src/components/Footer/Footer.css";

function Footer(){
    return(
        <>
            <div className="footer-background">
                <div className="footer-conteiner">
                    <div className="footer__info">
                        <p className="footer__info_header">&copy; ООО «<span className="text-logo">Мой</span>Маркет», 2018-2022.</p>
                        <p className="footer__info_text">Для уточнения информации звоните по номеру <a href="tel:79000000000">+7 900 000 0000</a>,</p>
                        <p className="footer__info_text">а предложения по сотрудничеству отправляйте на почту <a href="mailto:partner@mymarket.com" target="_blank">partner@mymarket.com</a></p>
                    </div>
                    <div className="footer__scroll">
                        <a href={"PageProduct"}>Наверх</a>
                    </div>
                </div>
            </div>
        </> 
    );
}
export default Footer;