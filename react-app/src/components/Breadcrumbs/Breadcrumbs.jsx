import React from "react";
import "../../../src/components/Breadcrumbs/Breadcrumbs.css";

function Breadcrumbs(){
    return(
        <>
            <div class="nav">
                <a href="./second.html">Электроника</a>
                <span>></span>
                <a href="#">Смартфоны и гаджеты</a>
                <span>></span>
                <a href="#">Мобильные телефоны</a>
                <span>></span>
                <a href="#">Apple</a>
            </div>
        </>
     );
}
    
export default Breadcrumbs;