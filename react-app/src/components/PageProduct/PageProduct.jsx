import React from "react";
import "../../../src/components/PageProduct/PageProduct.css";
import Iframe from "../../../src/components/Iframe/Iframe";
import Header from "../Header/Header";
import Breadcrumbs from "../Breadcrumbs/Breadcrumbs";
import Footer from "../Footer/Footer";
import product1 from "../../images/image-1.webp";
import product2 from "../../images/image-2.webp";
import product3 from "../../images/image-3.webp";
import product4 from "../../images/image-4.webp";
import product5 from "../../images/image-5.webp";
import color1 from "../../images/color-1.webp";
import color2 from "../../images/color-2.webp";
import color3 from "../../images/color-3.webp";
import color4 from "../../images/color-4.webp";
import color5 from "../../images/color-5.webp";
import color6 from "../../images/color-6.webp";
import star1 from "../../images/star.svg";
import star2 from "../../images/stargrey.svg";
import foto1 from "../../images/review-1.jpeg";
import foto2 from "../../images/review-2.jpeg";

function PageProduct(){
    return(
        <div className="body">
            <Header />
            <div className="conteiner">  
                <div className="wrapper">
                    <Breadcrumbs />
                    <main>
                        <h2 className="name">Смартфон Apple iPhone 13</h2>
                        <div className="apple">
                            <img className="apple__img" src={product1} alt="На переднем плане экран смартона,на заднем плане задняя панель смартфона"/>
                            <img className="apple__img" src={product2} alt="Фотография экрана смартфона"/>
                            <img className="apple__img" src={product3} alt="Фотография развёрнутого смартфона на 45 градусов"/>
                            <img className="apple__img" src={product4} alt="Фотография камер смартфона"/>
                            <img className="apple__img" src={product5} alt="На переднем плане задняя панель смартфона ,на заднем плане экран смартона"/>
                        </div>
                        <div className="produkt">
                            <div className="solid">
                                <div className="section">
                                    <h3>Цвет товара: Синий</h3>
                                    <div className="color">
                                        <div className="color__list">
                                                <img className="color__list_img" src={color1} alt="Фотография смартфона в красном цвете"/>
                                        </div>
                                        <div className="color__list">
                                            <img className="color__list_img" src={color2} alt="Фотография смартфона в зелёном цвете"/>
                                        </div>
                                        <div className="color__list">
                                            <img className="color__list_img" src={color3} alt="Фотография смартфона в розовом цвете"/>
                                        </div>
                                        <div className="color__list color__list_selected">
                                            <img className="color__list_img" src={color4} alt="Фотография смартфона в берюзовом цвете"/>
                                        </div>
                                        <div className="color__list">
                                            <img className="color__list_img" src={color5} alt="Фотография смартфона в белом цвете"/>
                                        </div>
                                        <div className="color__list">
                                            <img className="color__list_img" src={color6} alt="Фотография смартфона в чёрном цвете"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="section">
                                    <h3>Конфигурация памяти: 128 ГБ</h3>
                                    <div className="memory">
                                        <button className="memory__list memory__list_selected">128 ГБ</button>
                                        <button className="memory__list">256 ГБ</button>
                                        <button className="memory__list">512 ГБ</button>
                                    </div>
                                </div>
                                <div className="section">
                                    <h3>Характеристики товара</h3>
                                    <ul className="list">
                                        <li>Экран: <b>6.1</b></li>
                                        <li>Встроенная память: <b>128 ГБ</b></li>
                                        <li>Операционная система: <b>iOS 15</b></li>
                                        <li>Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
                                        <li>Процессор: <a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"><b>Apple A15 Bionic</b></a></li>
                                        <li>Вес: <b>173 г</b></li>
                                    </ul>
                                </div>
                                <div className="section">
                                    <h3>Описание</h3>
                                    <p>Наша самая совершенная система двух камер.<br/>
                                    Особый взгляд на прочность дисплея.<br/>
                                    Чип, с которым всё супербыстро.<br/>
                                    Аккумулятор держится заметно дольше.<br/>
                                    <i>iPhone 13 - сильный мира всего.</i></p>
                                    <p>Мы разработали совершенно новую схему расположения и развернулиобъективы на 45 градусов. Благодаря этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной камеры.Кроме того, мы освободили место для системы оптической стабилизацииизображения сдвигом матрицы. И повысили скорость работы матрицы насверхширокоугольной камере.</p>
                                    <p>Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая стабилизация сосдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                                    <p>Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая красивый эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.</p>
                                </div>
                                <div className="section__table">
                                    <h3>Сравнение моделей</h3>
                                    <table className="simile">
                                        <thead>
                                            <tr className="simile__header">
                                                <th className="simile__header simile__header_row">Модель</th>
                                                <th className="simile__header simile__header_row">Вес</th>
                                                <th className="simile__header simile__header_row">Высота</th>
                                                <th className="simile__header simile__header_row">Ширина</th>
                                                <th className="simile__header simile__header_row">Толщина</th>
                                                <th className="simile__header simile__header_row">Чип</th>
                                                <th className="simile__header simile__header_row">Объём памяти</th>
                                                <th className="simile__header simile__header_row">Аккумулятор</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="simile__tbody">
                                                <td className="simile__tbody simile__tbody_row">iPhone11</td>
                                                <td className="simile__tbody simile__tbody_row">194 грамма</td>
                                                <td className="simile__tbody simile__tbody_row">150.9 мм</td>
                                                <td className="simile__tbody simile__tbody_row">75.7 мм</td>
                                                <td className="simile__tbody simile__tbody_row"> 8.3 мм</td>
                                                <td className="simile__tbody simile__tbody_row">A13 Bionicchip</td>
                                                <td className="simile__tbody simile__tbody_row">до 128 Гб</td>
                                                <td className="simile__tbody simile__tbody_row">до 17 часов</td>
                                            </tr>
                                            <tr className="simile__tbody">
                                                <td className="simile__tbody simile__tbody_row">iPhone12</td>
                                                <td className="simile__tbody simile__tbody_row">164 грамма</td>
                                                <td className="simile__tbody simile__tbody_row">146.7 мм</td>
                                                <td className="simile__tbody simile__tbody_row">71.5 мм </td>
                                                <td className="simile__tbody simile__tbody_row"> 7.4 мм</td>
                                                <td className="simile__tbody simile__tbody_row">A14 Bionicchip</td>
                                                <td className="simile__tbody simile__tbody_row">до 256 Гб</td>
                                                <td className="simile__tbody simile__tbody_row">до 19 часов</td>
                                            </tr>
                                            <tr className="simile__tbody">
                                                <td className="simile__tbody simile__tbody_row">iPhone13</td>
                                                <td className="simile__tbody simile__tbody_row">174 грамма</td>
                                                <td className="simile__tbody simile__tbody_row">146.7 мм</td>
                                                <td className="simile__tbody simile__tbody_row">71.5 мм </td>
                                                <td className="simile__tbody simile__tbody_row">7.65 мм</td>
                                                <td className="simile__tbody simile__tbody_row">A15 Bionicchip</td>
                                                <td className="simile__tbody simile__tbody_row">до 512 Гб</td>
                                                <td className="simile__tbody simile__tbody_row">до 19 часов</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="review">
                                    <div className="review__header">
                                        <p><b>Отзывы</b></p>
                                        <span className="rewiew__header_number">425</span>
                                    </div>
                                    <div className="review__block">
                                        <div>
                                            <img className="review__block_foto" src={foto1} alt="Фотография мужчины"/>
                                        </div>
                                        <div className="review__block_info">
                                            <div className="review__block_header">
                                                <p className="review__block_header_name"><b>Марк Г.</b></p>
                                                <div>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                </div>
                                            </div>
                                            <div className="review__block_text">
                                                <p><b>Опыт использования:</b> менее месяца</p>
                                                <p><b>Достоинства:</b><br/>это мой первый айфон после после огромного количества телефонов на андроиде. всёплавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.</p>
                                                <p><b>Недостатки:</b><br/>к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное.</p>
                                            </div>
                                        </div>    
                                    </div>
                                    <div className="review__block">
                                        <div>
                                            <img className="review__block_foto" src={foto2} alt="Фотография мужчины"/>
                                        </div>
                                        <div className="review__block_info">
                                            <div className="review__block_header">
                                                <p className="review__block_header_name"><b>Валерий Коваленко</b></p>
                                                <div>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star1} alt="Звёзда"/>
                                                    <img className="review__block_header_star" src={star2} alt="Звёзда"/>
                                                </div>
                                            </div>
                                            <div className="review__block_text">
                                                <p><b>Опыт использования:</b> менее месяца</p>
                                                <p><b>Достоинства:</b><br/>OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</p>
                                                <p><b>Недостатки:</b><br/>Плохая ремонтопригодность</p>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                            <div className="aside">
                                <form className="price">
                                    <div className="price__top">
                                        <div className="price__old"><s className="price__old_cross-out">75 990₽</s><p className="price__sale">-8%</p></div>
                                        <label htmlFor="heard">
                                            <input className="checkbox" type="checkbox" id="heard" name="heard"/>
                                            <svg width="28" height="22" viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" clipRule="evenodd" d="M2.78502 2.57269C5.17872 0.274736 9.04661 0.274736 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.274736 22.8216 0.274736 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63043 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63043 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" />
                                            </svg>
                                        </label>
                                    </div>
                                    <div className="price__new"> 67 990₽</div>
                                    <div className="price__text">
                                        <div>Самовывоз в четверг, 1 сентября - <b>Бесплатно</b></div>
                                        <div>Курьером в четверг, 1 сентября - <b>Бесплатно</b></div>
                                    </div>
                                    <button className="price__btn" type="submit">Добавить в корзину</button>   
                                </form>
                                <div className="ad">
                                    <div className="ad_name">Реклама</div>
                                    <Iframe />
                                    <Iframe />
                                    
                                </div>
                            </div>
                        </div>
                        <form className="form">
                            <legend className="form__name">Добавить свой отзыв</legend>
                            <div className="form__main">
                                <div className="form__first">
                                    <label htmlFor="email">
                                        <input className="form__first_name" type="text" name="first-name" id="email" placeholder="Имя и фамилия"/>
                                        <div className="container-name_error"></div>
                                    </label>
                                    <label htmlFor="rating">
                                        <input className="form__first_rating" type="number" name="first-rating" id="rating" placeholder="Оценка"/>
                                        <div className="container-score_error"></div>
                                    </label>
                                </div>
                                <div>
                                    <textarea className="form__second" name="text-form" id="text" placeholder="Текст отзыва"></textarea>
                                </div>
                                <button type="submit" className="form__button">Отправить отзыв</button>
                            </div>
                        </form>
                    </main>
                </div>   
            </div>
            <Footer/>
        </div>
    );
}

export default PageProduct;