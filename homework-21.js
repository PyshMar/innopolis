"use scrict";

// Упражнение 1

/**
 * Подтверждает отсутствие свойст в объекте.
 * 
 * @param{object} obj проверяемый объект.
 * @return{object} true или false.
 */

function isEmpty(obj){
    for (let key in obj) {
        return false;
    }
    return true;
}

let obj = {};
isEmpty(obj);
alert(isEmpty(obj));


// Упражнение 3

let salaries= {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};
/**Увеличивает каждое значение в объекте на назначенный процент.
 * @param{number}perzent процент на который увеличиваем значение.
 * @return{number} значение увеличенное на назначенный perzent.
 */
function raiseSalary(perzent){
    let newSalaries = {};
    for (let key in salaries){
       newSalaries[key]= Math.floor(salaries[key] + (salaries[key]/100 * perzent));
    }
    return newSalaries;
}
let newSalaries = raiseSalary(5);
console.log(newSalaries);

/**Подсчитывает сумму свойств в объекте.
 * @param{object} используемый объект.
 * @return{number} сумма всех свойств объекта.
 */
function calcSumm(newSalaries){
    let summ=0;
    for (let key in newSalaries){
        summ = summ + newSalaries[key]; 
    }
    return summ; 
}
let summ = calcSumm(newSalaries);
console.log(summ);