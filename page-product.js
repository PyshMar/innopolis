"use scrict";

let form = document.querySelector('.form');

let inputName = form.querySelector ('.form__first_name');
let inputScore = form.querySelector('.form__first_rating');
let inputText = form.querySelector('.form__second');

let errorNameElem = form.querySelector('.container-name_error');
let errorScoreElem = form.querySelector('.container-score_error');


function handleSubmit(event) {
    event.preventDefault();
    let name = inputName.value;
    let score = inputScore.value;
    let errorName='';
    if(name.length === 0) {
        errorName = "Вы забыли указать имя и фамилию";
    }else if (name.length < 2) {
        errorName = "Имя не может быть короче 2-х символов";
    }
    errorNameElem.innerText = errorName;
    errorNameElem.classList.toggle("visible", errorName);
    inputName.classList.toggle("visible", errorName);
    if (errorName) return; 
    
    let errorScore='';
   
    if(score < 1 || score > 5) {
        errorScore = "Оценка должна быть от 1 до 5";
    }else if (Number.isNaN(score)) {
        errorScore = "Оценка должна быть от 1 до 5";
    }else if (score.length === 0) {
        errorScore = "Оценка должна быть от 1 до 5";
    }
    errorScoreElem.innerText = errorScore;
    errorScoreElem.classList.toggle("visible", errorScore);
    inputScore.classList.toggle("visible", errorScore);
    if (errorScore) return;
    if (!errorName) localStorage.removeItem('first-name');
    if (!errorName) localStorage.removeItem('first-rating');
    if (!errorName) localStorage.removeItem('text-form');
}

form.addEventListener('submit', handleSubmit);

inputName.addEventListener('input', () =>{
    errorNameElem.classList.remove("visible");
});
inputScore.addEventListener('input', () =>{
    errorScoreElem.classList.remove("visible");
});


function handleInput(event) {
    let value = event.target.value;
    let name = event.target.getAttribute("name");
    localStorage.setItem(name, value);
}
inputName.value = localStorage.getItem('first-name');
inputScore.value = localStorage.getItem('first-rating');
inputText.value = localStorage.getItem('text-form');

inputName.addEventListener('input', handleInput);
inputScore.addEventListener('input', handleInput);
inputText.addEventListener('input', handleInput);


let basketCart = document.querySelector('.basket__elements_amount');
let basketBtn = document.querySelector('.price__btn');
let cart = new Set();

function handleCart() {
    let nameBasket = localStorage.getItem('in cart:');
    if (nameBasket !== null){
        basketCart.style.display = 'block';
        basketCart.textContent = nameBasket;
        basketBtn.style.background = '#888888';
        basketBtn.textContent = 'Товар уже в корзине';  
    }else {
        basketCart.style.display = 'none';
        basketCart.textContent = nameBasket;
        basketBtn.textContent = 'Добавить в корзину';
    }
}

function addCart(evt) {
    evt.preventDefault();
    let nameBasket = localStorage.getItem('in cart:');
    if (nameBasket !== null){
        cart.delete(evt.target.id);
        localStorage.removeItem('in cart:');
        basketCart.style.display = 'none';
        basketCart.textContent = '';
        basketBtn.style.background = '';
        basketBtn.textContent = 'Добавить в корзину';   
    }else {
        cart.add(evt.target.id);
        localStorage.setItem('in cart:', cart.size);
        basketCart.style.display = 'block';
        basketCart.textContent = cart.size;
        basketBtn.style.background = '#888888';
        basketBtn.textContent = 'Товар уже в корзине';    
    }
}
handleCart();
basketBtn.addEventListener('click', addCart);
