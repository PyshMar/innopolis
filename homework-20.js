"use scrict";

//Упражнение 1

for (let i=0; i <= 20; i=i+2) {
if (i === 0) continue; 
console.log(i);
}

//Упражнение 2

let sum = 0;
let a = 0;
while (true && a < 3) {
a = a + 1;
let value = +prompt("Введите число", "");
sum += value;
  if (Number.isNaN(value)) {
  alert("Ошибка, вы ввели не число");
  break;
  }
}
alert("Сумма: " + sum);

// Упражнение 3

let month = +prompt('Введите месяц', ' ');
function getNameOfMonth(month){
    if ((month>=0) && (month<12)){
        let monthName = 'Январь,Февраль,Март,Апрель,Май,Июнь,Июль,Август,Сентябрь,Октябрь,Ноябрь,Декабрь';
        let count = monthName.split(',');
        return (count[`${month}`]);
        
    }
}
let result = getNameOfMonth(month);
alert (result);

// Цикл который выводит все месяца кроме октября
for (let b= 0; b<12 ; b++){
    if(b == 9) continue;
    console.log(getNameOfMonth(b));
}

//Упражнение 4
// IIFE («immediately-invoked function expressions»), что означает функцию, запускаемую сразу после объявления. 
// (function initGameIIFE() {
    // Весь код тут
// }());
// Скобки вокруг функции(лишь 1 из способов создать IIFE) – это трюк, который позволяет показать JavaScript, что функция была создана в контексте другого выражения, и, таким образом, это функциональное выражение: ей не нужно имя и её можно вызвать немедленно.
// Она используется для создания закрытой области видимости, и применяется в паттерне «модуль».
